const https = require('https');
const fs = require('fs');
const path = require('path');
const unzipper = require('unzipper');

const URL = 'https://api.beatsaver.com';
const TEMP_FOLD = path.join(__dirname, 'temp');

const link = process.argv[process.argv.length - 1];
const key = link.substr('beatsaver://'.length).split('/')[0];

function error(message){
    console.error("\x1b[31mERROR!!! ", message, '\x1b[0m');
}

function getUrl(url){
    return new Promise((resolve, reject) => {
        https.get(url, (res)=>{
            res.setEncoding('utf8');
            let rawData = '';
            res.on('data', (chunk)=>{
                rawData += chunk; 
                
            });
            res.on('end', ()=>{
                resolve(JSON.parse(rawData));
            });
            res.on('error', (e)=>{
                reject(e);
            })
        });
    });
}

function downloadFile(url, outputPath){
    console.log('Downloading: ',url, 'to', outputPath);
    return new Promise((resolve, reject) => {
        let file = fs.createWriteStream(outputPath);
        https.get(url, (res)=>{
            res.pipe(file);
            let total = res.headers['content-length'];
            let downloaded = 0;
            let lastPercent = 0;
            let percent = 0;
            res.on('end', ()=>{
                console.log(percent, '%');
                console.log('Download Completed');
                resolve();
            });
            res.on('data', (chunk) => {
                downloaded += chunk.length;
                percent = Math.floor(100*downloaded/total);
                if(percent - lastPercent > 5){
                    //console.log(percent, '%');
                    process.stdout.write(percent + '%' + '\r');
                    lastPercent = percent;
                }
               
            });
            res.on('error', (e)=>{
                reject(e);
            });
        });
    });
}

let config = {
    levelsFold : 'D:\\Steam\\steamapps\\common\\Beat Saber\\Beat Saber_Data\\CustomLevels\\'
}

if(!fs.existsSync('config.json')){
    fs.writeFileSync(path.join(__dirname, './config.json'), JSON.stringify(config, null, 4));
}else{
    config = fs.readFileSync(path.join(__dirname, './config.json'));
}

if(!fs.existsSync(TEMP_FOLD)){
    fs.mkdirSync(TEMP_FOLD);
}

getUrl(URL + '/maps/id/' + key).then((info)=>{
    console.log(info);
    let versions = info.versions;
    let url = null;
    let hash = null;
    for(let i=0;i<versions.length;i++){
        if(versions[i].key == key){
            url = versions[i].downloadURL;
            hash = versions[i].hash;
        }
    }
    
    if(url == null){
        error('Cannot find url to download');
        return;
    }
    let outputFolder = path.join(config.levelsFold, hash);
    if(fs.existsSync(outputFolder)){
        error('Level already loaded');
        return;
    }
    let zipPath = path.join(TEMP_FOLD, hash + '.zip');
    console.log(config.levelsFold);
    downloadFile(url, zipPath).then(()=>{
        console.log('Unzipping to ',outputFolder, '...');
        fs.createReadStream(zipPath).pipe(unzipper.Extract({ path: outputFolder }).on('close', ()=>{
            fs.unlinkSync(zipPath);
            console.log('\x1b[32mdone\x1b[0m');
        }));
    });
});
