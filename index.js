const path = require('path');

const ProtocolRegistry = require('protocol-registry');

const protocol='beatsaver';

console.log('Registering...');
// Registers the Protocol
ProtocolRegistry.register({
    protocol: protocol, 
    command: `node ${path.join(__dirname, './beatsaver-save.js')} $_URL_`, // this will be executed with a extra argument %url from which it was initiated
    override: true, // Use this with caution as it will destroy all previous Registrations on this protocol
    terminal: true, // Use this to run your command inside a terminal
    script: false
}).then(async () => {
    console.log('Successfully registered');
});